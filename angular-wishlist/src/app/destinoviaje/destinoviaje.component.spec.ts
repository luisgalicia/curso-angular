import { ComponentFixture, TestBed } from '@angular/core/testing';

import { destinoviajeComponent } from './destinoviaje.component';

describe('destinoviajeComponent', () => {
  let component: destinoviajeComponent;
  let fixture: ComponentFixture<destinoviajeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ destinoviajeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(destinoviajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
