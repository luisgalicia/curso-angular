import { Component, OnInit, Input } from '@angular/core';
import { destinoviaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destinoviaje.component.html',
  styleUrls: ['./destinoviaje.component.css']
})
export class destinoviajeComponent implements OnInit {
@Input() destino: destinoviaje;
  constructor() { }

  ngOnInit()  {
  }

}
