import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { destinoviaje } from './../models/destino-viaje.model';


@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class destinoviajeComponent implements OnInit {
@Input() destino: destinoviaje;
@HostBinding('attr.class') cssclass='col-md-6';
  constructor() {}
  

  ngOnInit() {
  }

}
